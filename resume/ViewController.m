//
//  ViewController.m
//  resume
//
//  Created by zufa on 3/9/13.
//  Copyright (c) 2013 Sichkar Evgen. All rights reserved.
//

#import "ViewController.h"
#import "Resume.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize scrollView=_scrollView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    _scrollView.delegate = self;
    _scrollView.contentSize = CGSizeMake(612, 792);
    _scrollView.userInteractionEnabled = YES;
    _scrollView.scrollEnabled = YES;
    
    Resume *qeew = [[Resume alloc] init];
    _scrollView.contentSize = CGSizeMake(qeew.frame.size.height, qeew.frame.size.width);
    [_scrollView addSubview:qeew];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
