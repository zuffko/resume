
#import "DesignMarking.h"

CGSize const resumeSize = {612.0f ,792.0f};
float const headHeight = 120.0f;
CGSize const photoSise = {100.0f ,100.0f};
float const nameAndPositionMaxWeight = 335.0f;
float const topAndBottomBackgroungHeigth = 20.0;
float const basicIndent = 10.0f;
float const lineHeight = 1.0f;
float const longLineWeight = 572.0f;
float const shortLineWeight = 412.0f;
float const infoBlockTitleMaxWeight = 180.0f;
float const temporaryHeight = 20.0f;
float const contactDataMaxHeight = 160.0f;
float const plusInfoPositionMaxWeight = 300.0f;

@implementation DesignMarking

@end