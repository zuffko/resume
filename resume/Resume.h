//
//  Resume.h
//  resume
//
//  Created by zufa on 3/9/13.
//  Copyright (c) 2013 Sichkar Evgen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Resume : UIView


@property (nonatomic, retain) UILabel *date;
@property (nonatomic, retain) UILabel *prevPost;
@property (nonatomic, retain) UILabel *title;

@end
