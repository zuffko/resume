//
//  InfoBlockView.h
//  resume
//
//  Created by zufa on 4/9/13.
//  Copyright (c) 2013 Sichkar Evgen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoBlockView : UIView{
    UILabel *title;
    UITextView *description;
}
@property (nonatomic, retain) UILabel *title;
@property (nonatomic, retain) UITextView *description;
@end
