//
//  HeadViev.h
//  resume
//
//  Created by zufa on 4/9/13.
//  Copyright (c) 2013 Sichkar Evgen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeadViev : UIScrollView

@property (nonatomic, retain) UILabel *nameLbel;
@property (nonatomic, retain) UILabel *email;
@property (nonatomic, retain) UILabel *phoneNumber;
@property (nonatomic, retain) UILabel *position;
@property (nonatomic, retain) UILabel *adress;
@property (nonatomic, retain) UIImageView *picture;

@end
