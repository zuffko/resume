//
//  InfoBlocPlusView.h
//  resume
//
//  Created by zufa on 7/9/13.
//  Copyright (c) 2013 Sichkar Evgen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InfoBlockView.h"

@interface InfoBlocPlusView : InfoBlockView
@property (nonatomic, retain) UILabel *position;
@property (nonatomic, retain) UITextView *positionDescription;
@property (nonatomic, retain) UILabel *date;
@property (nonatomic, retain) UIView *dateBackground;

@end
