//
//  HeadViev.m
//  resume
//
//  Created by zufa on 4/9/13.
//  Copyright (c) 2013 Sichkar Evgen. All rights reserved.
//

#import "HeadViev.h"
#import <QuartzCore/QuartzCore.h>
#import "ResumeModel.h"

@implementation HeadViev
@synthesize nameLbel;
@synthesize email;
@synthesize phoneNumber;
@synthesize position;
@synthesize adress;
@synthesize picture;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setNeedsDisplay
{
    self.layer.borderWidth = 1;
    self.layer.borderColor = [UIColor colorWithRed:203.0f/255.0f green:203.0f/255.0f blue:203.0f/255.0f alpha:1.0f].CGColor;
    self.layer.backgroundColor = [UIColor colorWithRed:241.0f/255.0f green:241.0f/255.0f blue:241.0f/255.0f alpha:1.0f].CGColor;

    nameLbel = [[UILabel alloc] init];
    nameLbel.font = [UIFont systemFontOfSize:30];
    nameLbel.backgroundColor = [UIColor clearColor];
    [self addSubview:nameLbel];
    email = [[UILabel alloc] init];
    email.font =phoneNumber.font =adress.font = [UIFont systemFontOfSize:15];
    email.backgroundColor = [UIColor clearColor];
    [self addSubview:email];
    phoneNumber = [[UILabel alloc] init];
    phoneNumber.backgroundColor = [UIColor clearColor];
    [self addSubview:phoneNumber];
    position = [[UILabel alloc] init];
    position.font = [UIFont systemFontOfSize:30];
    position.backgroundColor = [UIColor clearColor];
    [self addSubview:position];
    adress = [[UILabel alloc] init];
    adress.backgroundColor = [UIColor clearColor];
    adress.font = [UIFont systemFontOfSize:15];
    [self addSubview:adress];
    picture = [[UIImageView alloc] init];
    [self addSubview:picture];
}


@end
