//
//  ResumeModel.h
//  resume
//
//  Created by zufa on 4/9/13.
//  Copyright (c) 2013 Sichkar Evgen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ResumeModel : NSObject{
    NSString *name;
    NSString *position;
    NSString *email;
    NSString *phoneNumber;
    NSString *adress;
    NSString *professionalProfileTitle;
    UIImage  *picture;
}

@property (nonatomic, retain)    NSString *name;
@property (nonatomic, retain)    NSString *position;
@property (nonatomic, retain)    NSString *email;
@property (nonatomic, retain)    NSString *phoneNumber;
@property (nonatomic, retain)    NSString *adress;
@property (nonatomic, retain)    UIImage  *picture;
@property (nonatomic, retain)    NSString *professionalProfileTitle;
@property (nonatomic, retain)    NSString *professionalProfileDescription;
@property (nonatomic, retain)   NSString *plusTitle;
@property (nonatomic, retain)   NSString *plusposition;
@property (nonatomic, retain)   NSString *plusDate;
@property (nonatomic, retain)   NSString *plusposition2;
@property (nonatomic, retain)   NSString *plusDate2;
@property (nonatomic, retain)   NSString *plusDesc;
@property (nonatomic, retain)   NSString *plusDesc2;


-(void)setResumeData;

@end
