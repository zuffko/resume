//
//  InfoBlockView.m
//  resume
//
//  Created by zufa on 4/9/13.
//  Copyright (c) 2013 Sichkar Evgen. All rights reserved.
//

#import "InfoBlockView.h"

@implementation InfoBlockView
@synthesize title;
@synthesize description;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)setNeedsDisplay
{
    title = [[UILabel alloc] init];
    title.font=[UIFont systemFontOfSize:18];
    title.backgroundColor = [UIColor clearColor];
    [self addSubview:title];
    description = [[UITextView alloc] init];
    description.font = [UIFont systemFontOfSize:15];
    description.backgroundColor = [UIColor clearColor];
    [self addSubview:description];
}

@end
