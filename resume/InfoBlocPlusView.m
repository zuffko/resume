//
//  InfoBlocPlusView.m
//  resume
//
//  Created by zufa on 7/9/13.
//  Copyright (c) 2013 Sichkar Evgen. All rights reserved.
//

#import "InfoBlocPlusView.h"

@implementation InfoBlocPlusView
@synthesize position;
@synthesize positionDescription;
@synthesize date;
@synthesize dateBackground;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void) setNeedsDisplay
{
    title = [UILabel new];
    title.font=[UIFont systemFontOfSize:18];
    title.backgroundColor = [UIColor clearColor];
    [self addSubview:title];
    position = [UILabel new];
    position.font=[UIFont systemFontOfSize:15];
    position.backgroundColor = [UIColor clearColor];
    [self addSubview:position];
    positionDescription = [UITextView new];
    positionDescription.font= [UIFont systemFontOfSize:15];
    positionDescription.backgroundColor = [UIColor clearColor];
    [self addSubview:positionDescription];
    dateBackground = [UIView new];
    dateBackground.backgroundColor = [UIColor grayColor];
    [self addSubview:dateBackground];
    date = [UILabel new];
    date.backgroundColor = [UIColor clearColor];
    date.font = [UIFont systemFontOfSize:17];
    [self addSubview:date];
    
}

@end
