//
//  AppDelegate.h
//  resume
//
//  Created by zufa on 3/9/13.
//  Copyright (c) 2013 Sichkar Evgen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
