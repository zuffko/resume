//
//  Resume.m
//  resume
//
//  Created by zufa on 3/9/13.
//  Copyright (c) 2013 Sichkar Evgen. All rights reserved.
//

#import "Resume.h"
#import "HeadViev.h"
#import "InfoBlockView.h"
#import "ResumeModel.h"
#import "InfoBlocPlusView.h"
#import "DesignMarking.h"

@implementation Resume{
    ResumeModel *jonnResume;
    UIView *line;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

    }
    return self;
}

- (void)setNeedsDisplay
{
    self.frame = CGRectMake(0, 0, resumeSize.height, resumeSize.width);
    
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, resumeSize.height, topAndBottomBackgroungHeigth)];
    topView.backgroundColor = [UIColor colorWithRed:234.0f/255.0f green:170.0f/255.0f blue:53.0f/255.0f alpha:1.0f];
    [self addSubview:topView];
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, resumeSize.height-topAndBottomBackgroungHeigth, resumeSize.width, topAndBottomBackgroungHeigth)];
    bottomView.backgroundColor = [UIColor colorWithRed:234.0f/255.0f green:170.0f/255.0f blue:53.0f/255.0f alpha:1.0f];
    [self addSubview:bottomView];
    
    jonnResume = [ResumeModel new];
    [jonnResume setResumeData];
    
    [self createHead];
    [self createInfoblock];
    [self creatrInfoBlockPlus];
    
    [self newList];
}

-(void)createHead
{
    HeadViev *head = [[HeadViev alloc] initWithFrame:CGRectMake(0, topAndBottomBackgroungHeigth, resumeSize.width, headHeight)];
    head.email.text = jonnResume.email;
    head.picture.frame = CGRectMake(basicIndent, basicIndent, photoSise.width, photoSise.height);
    head.picture.image = jonnResume.picture;
    head.nameLbel.text = jonnResume.name;
    CGSize nameSize=[jonnResume.name sizeWithFont:head.nameLbel.font constrainedToSize:CGSizeMake(MAXFLOAT, MAXFLOAT) lineBreakMode:NSLineBreakByCharWrapping];
    [self stringWeight:jonnResume.name inLabel:head.nameLbel maxWeight:nameAndPositionMaxWeight];
    head.nameLbel.frame = CGRectMake(head.picture.frame.size.width+head.picture.frame.origin.x+basicIndent, (head.frame.size.height/2.0)-nameSize.height, nameAndPositionMaxWeight, temporaryHeight);
    [head.nameLbel sizeToFit];
    head.position.text = jonnResume.position;
    [self stringWeight:jonnResume.position inLabel:head.position maxWeight:nameAndPositionMaxWeight];
    head.position.frame = CGRectMake(head.picture.frame.size.width+(basicIndent*2), head.nameLbel.frame.size.height+(basicIndent*2), nameAndPositionMaxWeight, temporaryHeight);
    [head.position sizeToFit];
    head.phoneNumber.text = jonnResume.phoneNumber;
    head.adress.text = jonnResume.adress;
    CGSize emailSize=[jonnResume.email sizeWithFont:head.email.font constrainedToSize:CGSizeMake(contactDataMaxHeight, MAXFLOAT) lineBreakMode:NSLineBreakByCharWrapping];
    CGSize phoneNumberSize=[jonnResume.phoneNumber sizeWithFont:head.phoneNumber.font constrainedToSize:CGSizeMake(contactDataMaxHeight, MAXFLOAT) lineBreakMode:NSLineBreakByCharWrapping];
    CGSize adressSize=[jonnResume.adress sizeWithFont:head.adress.font constrainedToSize:CGSizeMake(contactDataMaxHeight, MAXFLOAT) lineBreakMode:NSLineBreakByCharWrapping];
    head.email.text=jonnResume.email;
    head.adress.text=jonnResume.adress;
    head.phoneNumber.text = jonnResume.phoneNumber;
    NSArray *stringSizeArray = [NSArray arrayWithObjects:[NSNumber numberWithFloat:emailSize.width], [NSNumber numberWithFloat:phoneNumberSize.width], [NSNumber numberWithFloat:adressSize.width], nil];
    NSNumber *maxWeightValue = [stringSizeArray valueForKeyPath:@"@max.self"];
    float longestStringWeight = [maxWeightValue floatValue]/2.0;
    head.email.frame = CGRectMake(resumeSize.width-longestStringWeight-(emailSize.width/2)-basicIndent, basicIndent, emailSize.width, emailSize.height);
    head.phoneNumber.frame = CGRectMake(resumeSize.width-longestStringWeight-(phoneNumberSize.width/2)-basicIndent, basicIndent+emailSize.height+basicIndent, phoneNumberSize.width, phoneNumberSize.height);
    head.adress.frame = CGRectMake(resumeSize.width-longestStringWeight-(adressSize.width/2.0)-basicIndent, head.phoneNumber.frame.origin.y+head.phoneNumber.frame.size.height +basicIndent, adressSize.width+(basicIndent*2), adressSize.height);
    head.adress.numberOfLines=2;
    [head.adress sizeToFit];
    [self addSubview:head];
}

- (void)createInfoblock
{
    InfoBlockView *infoBlock = [[InfoBlockView alloc] initWithFrame:CGRectMake(0, topAndBottomBackgroungHeigth+headHeight, resumeSize.width, temporaryHeight)];
    [self addSubview:infoBlock];
    infoBlock.title.text = jonnResume.professionalProfileTitle;
    infoBlock.title.frame = CGRectMake(basicIndent, basicIndent, infoBlockTitleMaxWeight, temporaryHeight);
    [self stringWeight:jonnResume.professionalProfileTitle inLabel:infoBlock.title maxWeight:infoBlockTitleMaxWeight];
    infoBlock.description.text = jonnResume.professionalProfileDescription;
    infoBlock.description.frame = CGRectMake(infoBlockTitleMaxWeight+(basicIndent*2.0), basicIndent, resumeSize.width-infoBlockTitleMaxWeight-(basicIndent*2.0), temporaryHeight);
    CGRect frame = infoBlock.description.frame;
    frame.size.height = infoBlock.description.contentSize.height;
    infoBlock.description.frame = frame;
    infoBlock.frame = CGRectMake(0, topAndBottomBackgroungHeigth+headHeight, resumeSize.width, infoBlock.description.frame.size.height+infoBlock.description.frame.origin.y);
    line = [[UIView alloc] initWithFrame:CGRectMake((basicIndent*2), infoBlock.frame.origin.y+infoBlock.frame.size.height, longLineWeight,lineHeight)];
    line.backgroundColor = [UIColor blackColor];
    [self addSubview:line];
}

-(void)creatrInfoBlockPlus
{
    InfoBlocPlusView *plusInfo = [[InfoBlocPlusView alloc] initWithFrame:CGRectMake(0, line.frame.origin.y+lineHeight, resumeSize.width, temporaryHeight)];
    [self addSubview:plusInfo];
    plusInfo.title.text= jonnResume.plusTitle;
    plusInfo.title.frame = CGRectMake(basicIndent, basicIndent, infoBlockTitleMaxWeight, temporaryHeight);
    [self stringWeight:jonnResume.plusTitle inLabel:plusInfo.title maxWeight:infoBlockTitleMaxWeight];
    plusInfo.position.text = jonnResume.plusposition;
    plusInfo.position.frame = CGRectMake(infoBlockTitleMaxWeight+(basicIndent*2.0), basicIndent, resumeSize.width-infoBlockTitleMaxWeight-(basicIndent*2.0), temporaryHeight);
    [self stringWeight:jonnResume.plusposition inLabel:plusInfo.position maxWeight:plusInfoPositionMaxWeight];
    [plusInfo.position sizeToFit];
    plusInfo.date.text = jonnResume.plusDate;
    CGSize dateSize=[jonnResume.plusDate sizeWithFont:plusInfo.date.font constrainedToSize:CGSizeMake(MAXFLOAT, MAXFLOAT) lineBreakMode:NSLineBreakByCharWrapping];
    plusInfo.date.frame = CGRectMake(resumeSize.width-basicIndent-dateSize.width, basicIndent, dateSize.width, dateSize.height);
    plusInfo.positionDescription.text = jonnResume.plusDesc;
    plusInfo.positionDescription.frame = CGRectMake(infoBlockTitleMaxWeight+(basicIndent*2.0), plusInfo.date.frame.origin.y+plusInfo.date.frame.size.height+basicIndent,resumeSize.width-infoBlockTitleMaxWeight-(basicIndent*3.0), temporaryHeight);
    CGRect frame2 = plusInfo.positionDescription.frame;
    frame2.size.height = plusInfo.positionDescription.contentSize.height;
    plusInfo.positionDescription.frame = frame2;
    plusInfo.dateBackground.frame = CGRectMake(plusInfo.date.frame.origin.x-2, plusInfo.date.frame.origin.y-2, plusInfo.date.frame.size.width+4, plusInfo.date.frame.size.height+4);
    UIView *infoBlockPlusline = [[UIView alloc] initWithFrame:CGRectMake((basicIndent*2)+infoBlockTitleMaxWeight,  plusInfo.frame.origin.y+plusInfo.frame.size.height+plusInfo.date.frame.size.height, shortLineWeight-basicIndent*2,lineHeight)];
    infoBlockPlusline.backgroundColor = [UIColor blackColor];
    [self addSubview:infoBlockPlusline];
    plusInfo.frame=CGRectMake(plusInfo.frame.origin.x, plusInfo.frame.origin.y, plusInfo.frame.size.height, plusInfo.positionDescription.frame.origin.y+plusInfo.positionDescription.frame.size.height);
    UIView *infoBlockPluslineBottom = [[UIView alloc] initWithFrame:CGRectMake((basicIndent*2)+infoBlockTitleMaxWeight,  plusInfo.frame.origin.y+plusInfo.frame.size.height, shortLineWeight-basicIndent*2,lineHeight)];
    infoBlockPluslineBottom.backgroundColor = [UIColor blackColor];
    [self addSubview:infoBlockPluslineBottom];
}

- (void) stringWeight:(NSString*)string inLabel:(UILabel*)label maxWeight:(float)maxWeight
{
    CGSize positionSize=[string sizeWithFont:label.font constrainedToSize:CGSizeMake(MAXFLOAT, MAXFLOAT) lineBreakMode:NSLineBreakByCharWrapping];
    if (positionSize.width>maxWeight) {
        while ([string sizeWithFont:label.font constrainedToSize:CGSizeMake(MAXFLOAT, MAXFLOAT) lineBreakMode:NSLineBreakByCharWrapping].width>maxWeight) {
            label.font=[label.font fontWithSize:label.font.pointSize-1];
        }
    }
}

- (void)newList
{

}

@end

